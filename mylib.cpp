#include <iostream>;
#include <mylib.h>;

using namespace std;

int Fun() {
    int num;

    cout << "Enter value: ";
    cin >> num;

    int d_arr = new int[num];

    for (int i = 0; i < num; i++) {
        d_arr[i] = i;

        cout << "Value of" << i << " element is " << d_arr[i] << endl;
    }

    delete [] d_arr;
}
